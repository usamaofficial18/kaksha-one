import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListingComponent } from './shared-ui/listing/listing.component';
import { AuthGuard } from './common/guards/auth-guard/auth.guard.service';
import { HomeComponent } from './shared-ui/home/home.component';
import { DashboardComponent } from './shared-ui/dashboard/dashboard.component';
import { OrganizationComponent } from './education/components/organization/organization.component';

const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'dashboard', component: DashboardComponent },
  {
    path: 'list/:model',
    component: ListingComponent,
    canActivate: [AuthGuard],
  },
  { path: 'organization', component: OrganizationComponent },

  // IMPORTANT: Keep this at the end of array
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: '**', redirectTo: 'home' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
