import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { OAuthModule, OAuthStorage } from 'angular-oauth2-oidc';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SharedUIModule } from './shared-ui/shared-ui.module';
import { AppService } from './app.service';
import { EducationModule } from './education/education.module';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    OAuthModule.forRoot(),
    SharedUIModule,
    EducationModule,
  ],
  providers: [AppService, { provide: OAuthStorage, useValue: localStorage }],
  bootstrap: [AppComponent],
})
export class AppModule {}
