import { IsOptional } from 'class-validator';

export class ListQueryDto {
  @IsOptional()
  offset: number;

  @IsOptional()
  limit: number;

  @IsOptional()
  search: string;

  @IsOptional()
  // @IsInstance(QuerySort)
  sort: string;

  @IsOptional()
  filter_query: string;
}
