import { ICommand } from '@nestjs/cqrs';

export class RemoveSubjectCommand implements ICommand {
  constructor(public readonly uuid: string) {}
}
