import { ICommand } from '@nestjs/cqrs';
import { UpdateSubjectDto } from '../../../entities/subject/subject.dto';

export class UpdateSubjectCommand implements ICommand {
  constructor(
    public readonly updatePayload: UpdateSubjectDto,
    public readonly clientHttpRequest: any,
  ) {}
}
