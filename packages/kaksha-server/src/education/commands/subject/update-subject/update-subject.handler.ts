import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { SubjectAggregateService } from '../../../aggregates/subject/subject-aggregate.service';
import { UpdateSubjectCommand } from './update-subject.command';

@CommandHandler(UpdateSubjectCommand)
export class UpdateSubjectHandler
  implements ICommandHandler<UpdateSubjectCommand> {
  constructor(
    private publisher: EventPublisher,
    private manager: SubjectAggregateService,
  ) {}

  async execute(command: UpdateSubjectCommand) {
    const { updatePayload, clientHttpRequest } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await this.manager.updateSubject(updatePayload, clientHttpRequest);
    aggregate.commit();
    return updatePayload;
  }
}
