import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { SubjectAggregateService } from '../../../aggregates/subject/subject-aggregate.service';
import { AddSubjectCommand } from './add-subject.command';

@CommandHandler(AddSubjectCommand)
export class AddSubjectHandler implements ICommandHandler<AddSubjectCommand> {
  constructor(
    private publisher: EventPublisher,
    private manager: SubjectAggregateService,
  ) {}
  async execute(command: AddSubjectCommand) {
    const { subjectPayload, clientHttpRequest } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    const subject = await aggregate.addSubject(
      subjectPayload,
      clientHttpRequest,
    );
    aggregate.commit();
    return subject;
  }
}
