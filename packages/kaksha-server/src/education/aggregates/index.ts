import { OrganizationAggregateService } from './organization-aggregate/organization-aggregate.service';
import { SubjectAggregateService } from './subject/subject-aggregate.service';

export const EducationAggregates = [
  SubjectAggregateService,
  OrganizationAggregateService,
];
