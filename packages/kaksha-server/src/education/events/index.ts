import { OrganizationSetupCompletedHandler } from './organization-setup-completed/organization-setup-completed.handler';
import { OrganizationUpdatedHandler } from './organization-updated/organization-updated.handler';

export const EducationEventHandlers = [
  OrganizationSetupCompletedHandler,
  OrganizationUpdatedHandler,
];
