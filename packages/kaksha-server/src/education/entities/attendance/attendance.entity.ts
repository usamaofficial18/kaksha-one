import { Entity, BaseEntity, ObjectIdColumn, Column, ObjectID } from 'typeorm';
import { v4 as uuidv4 } from 'uuid';

export enum Attended {
  Present = 'present',
  Absent = 'absent',
}

@Entity()
export class Attendance extends BaseEntity {
  @ObjectIdColumn()
  _id: ObjectID;

  @Column()
  uuid: string;

  @Column()
  date: Date;

  @Column()
  attendance: Attended;

  constructor() {
    super();
    if (!this.uuid) this.uuid = uuidv4();
  }
}
