import { SettingsService } from './settings/settings.service';
import { HealthCheckAggregateService } from './health-check/health-check.service';

export const SystemSettingsAggregates = [
  SettingsService,
  HealthCheckAggregateService,
];
